<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/prolog_user.php");
if (!function_exists('DeclOfNum')) {
    function DeclOfNum($number, $titles) {
        $cases = array(2, 0, 1, 1, 1, 2);
        return $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }
}

if (!($USER->CanDoOperation('view_subordinate_users') || $USER->CanDoOperation('view_all_users')))
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/user_admin.php");
IncludeModuleLangFile(__FILE__);

$FN = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST["FN"]);
$FC = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST["FC"]);
if ($FN == "")
    $FN = "find_form";
if ($FC == "")
    $FC = "USER_ID";

if (isset($_REQUEST['JSFUNC'])) {
    $JSFUNC = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST['JSFUNC']);
} else {
    $JSFUNC = '';
}
// идентификатор таблицы
$sTableID = "tbl_user_popup";

// инициализация сортировки
$oSort = new CAdminSorting($sTableID, "ID", "asc");
// инициализация списка
$lAdmin = new CAdminList($sTableID, $oSort);


if (!$USER->CanDoOperation('view_all_users')) {
    $arUserSubordinateGroups = array();
    $arUserGroups = CUser::GetUserGroup($USER->GetID());
    foreach ($arUserGroups as $grp)
        $arUserSubordinateGroups = array_merge($arUserSubordinateGroups, CGroup::GetSubordinateGroups($grp));

    $arFilter["CHECK_SUBORDINATE"] = array_unique($arUserSubordinateGroups);
}
if (isset($_REQUEST['GENDER']) && strlen($_REQUEST['GENDER']) > 0) {
    if($_REQUEST['GENDER'] == 'N'){
        $arFilter['PERSONAL_GENDER'] = false;
    }
    else{
       $arFilter['PERSONAL_GENDER'] = $_REQUEST['GENDER']; 
    }
    
}
if (isset($_REQUEST['CVP']) && is_array($_REQUEST['CVP'])) {
    $arFilter['UF_PRISES'] = $_REQUEST['CVP'];
}
if (isset($_REQUEST['DATE_REGISTER_FROM']) && strlen($_REQUEST['DATE_REGISTER_FROM']) > 0) {
    $arFilter['DATE_REGISTER_1'] = $_REQUEST['DATE_REGISTER_FROM'];
}
if (isset($_REQUEST['DATE_REGISTER_TO']) && strlen($_REQUEST['DATE_REGISTER_TO']) > 0) {
    $arFilter['DATE_REGISTER_2'] = $_REQUEST['DATE_REGISTER_TO'];
}
if ($REQUEST_METHOD == "POST") {

    $rsData = CUser::GetList($by, $order, $arFilter);
    $count = $rsData->SelectedRowsCount();
    $request = http_build_query($_REQUEST);
    ?>
<a onclick="window.open('<?php echo $APPLICATION->GetCurPageParam($request, array_keys($_REQUEST)); ?>', '', 'scrollbars=yes,resizable=yes,width=760,height=500,top='+Math.floor((screen.height - 560)/2-14)+',left='+Math.floor((screen.width - 760)/2-5)); return false;" href="#"><?php echo DeclOfNum($count,array('Найден','Найдено','Найдено')); ?> <?php echo $count; ?> <?php echo DeclOfNum($count,array('пользователь','пользователя','пользователей')); ?></a>
    <?php
    die;
} else {
    // инициализация списка - выборка данных
    $rsData = CUser::GetList($by, $order, $arFilter, array(
                "NAV_PARAMS" => array("nPageSize" => CAdminResult::GetNavSize($sTableID)),
    ));
    $rsData = new CAdminResult($rsData, $sTableID);
    $rsData->NavStart();
}
// установке параметров списка
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("PAGES")));
// заголовок списка
$lAdmin->AddHeaders(array(
    array("id" => "NUMBER", "content" => "№", "sort" => "", "default" => true),
    array("id" => "ID", "content" => "ID", "sort" => "id", "default" => true),
    array("id" => "TIMESTAMP_X", "content" => GetMessage('TIMESTAMP'), "sort" => "timestamp_x", "default" => true),
    array("id" => "ACTIVE", "content" => GetMessage('ACTIVE'), "sort" => "active", "default" => true),
    array("id" => "LOGIN", "content" => GetMessage("LOGIN"), "sort" => "login", "default" => true),
    array("id" => "NAME", "content" => GetMessage("NAME"), "sort" => "name", "default" => true),
    array("id" => "LAST_NAME", "content" => GetMessage("LAST_NAME"), "sort" => "last_name", "default" => true),
    array("id" => "EMAIL", "content" => GetMessage('EMAIL'), "sort" => "email", "default" => true),
    array("id" => "LAST_LOGIN", "content" => GetMessage("LAST_LOGIN"), "sort" => "last_login", "default" => true),
    array("id" => "DATE_REGISTER", "content" => GetMessage("DATE_REGISTER"), "sort" => "date_register"),
    array("id" => "PERSONAL_BIRTHDAY", "content" => GetMessage("PERSONAL_BIRTHDAY"), "sort" => "personal_birthday"),
    array("id" => "PERSONAL_PROFESSION", "content" => GetMessage("PERSONAL_PROFESSION"), "sort" => "personal_profession"),
    array("id" => "PERSONAL_WWW", "content" => GetMessage("PERSONAL_WWW"), "sort" => "personal_www"),
    array("id" => "PERSONAL_ICQ", "content" => GetMessage("PERSONAL_ICQ"), "sort" => "personal_icq"),
    array("id" => "PERSONAL_GENDER", "content" => GetMessage("PERSONAL_GENDER"), "sort" => "personal_gender"),
    array("id" => "PERSONAL_PHONE", "content" => GetMessage("PERSONAL_PHONE"), "sort" => "personal_phone"),
    array("id" => "PERSONAL_MOBILE", "content" => GetMessage("PERSONAL_MOBILE"), "sort" => "personal_mobile"),
    array("id" => "PERSONAL_CITY", "content" => GetMessage("PERSONAL_CITY"), "sort" => "personal_city"),
    array("id" => "PERSONAL_STREET", "content" => GetMessage("PERSONAL_STREET"), "sort" => "personal_street"),
    array("id" => "WORK_COMPANY", "content" => GetMessage("WORK_COMPANY"), "sort" => "work_company"),
    array("id" => "WORK_DEPARTMENT", "content" => GetMessage("WORK_DEPARTMENT"), "sort" => "work_department"),
    array("id" => "WORK_POSITION", "content" => GetMessage("WORK_POSITION"), "sort" => "work_position"),
    array("id" => "WORK_WWW", "content" => GetMessage("WORK_WWW"), "sort" => "work_www"),
    array("id" => "WORK_PHONE", "content" => GetMessage("WORK_PHONE"), "sort" => "work_phone"),
    array("id" => "WORK_CITY", "content" => GetMessage("WORK_CITY"), "sort" => "work_city"),
    array("id" => "XML_ID", "content" => GetMessage("XML_ID"), "sort" => "external_id"),
    array("id" => "EXTERNAL_AUTH_ID", "content" => GetMessage("EXTERNAL_AUTH_ID")),
));

// построение списка
$k=($rsData->NavPageNomer*$rsData->NavPageSize) - $rsData->NavPageSize + 1;
while ($arRes = $rsData->GetNext()) {
    $f_ID = $arRes['ID'];
    $row = & $lAdmin->AddRow($f_ID, $arRes);
    $row->AddViewField("NUMBER", $k);
    $row->AddViewField("ID", $f_ID);
    $row->AddCheckField("ACTIVE", false);
    $row->AddViewField("LOGIN", $arRes["LOGIN"]);
    $row->AddViewField("NAME", $arRes["NAME"]);
    $row->AddViewField("LAST_NAME", $arRes["LAST_NAME"]);
    $row->AddViewField("EMAIL", $arRes["EMAIL"]);
    $row->AddViewField("PERSONAL_PROFESSION", $arRes[""]);
    $row->AddViewField("PERSONAL_WWW", TxtToHtml($arRes["PERSONAL_WWW"]));
    $row->AddViewField("PERSONAL_ICQ", $arRes["PERSONAL_ICQ"]);
    $row->AddViewField("PERSONAL_GENDER", $arRes["PERSONAL_GENDER"]);
    $row->AddViewField("PERSONAL_PHONE", $arRes["PERSONAL_PHONE"]);
    $row->AddViewField("PERSONAL_MOBILE", $arRes["PERSONAL_MOBILE"]);
    $row->AddViewField("PERSONAL_CITY", $arRes["PERSONAL_CITY"]);
    $row->AddViewField("PERSONAL_STREET", $arRes["PERSONAL_STREET"]);
    $row->AddViewField("WORK_COMPANY", $arRes["WORK_COMPANY"]);
    $row->AddViewField("WORK_DEPARTMENT", $arRes["WORK_DEPARTMENT"]);
    $row->AddViewField("WORK_POSITION", $arRes["WORK_POSITION"]);
    $row->AddViewField("WORK_WWW", TxtToHtml($arRes["WORK_WWW"]));
    $row->AddViewField("WORK_PHONE", $arRes["WORK_PHONE"]);
    $row->AddViewField("WORK_CITY", $arRes["WORK_CITY"]);
    $row->AddViewField("XML_ID", $arRes["XML_ID"]);

    $arActions = array();
    $arActions[] = array(
        "ICON" => "",
        "TEXT" => GetMessage("MAIN_CHANGE"),
        "DEFAULT" => true,
        "ACTION" => "SetValue('" . $f_ID . "');"
    );
    //$row->AddActions($arActions);
    $k++;
}

// "подвал" списка
$lAdmin->AddFooter(
        array(
            array("title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsData->SelectedRowsCount()),
            array("counter" => true, "title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"),
        )
);

$lAdmin->AddAdminContextMenu(array());

// проверка на вывод только списка (в случае списка, скрипт дальше выполняться не будет)
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("MAIN_PAGE_TITLE"));
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php")
?>
<?
// место для вывода списка
if ($REQUEST_METHOD == "POST") {
    $APPLICATION->RestartBuffer();
} else {
    $lAdmin->DisplayList();
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");
}
?>
